﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_1

{
    class Dataset:Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }
        public Prototype Clone()
        {
            Dataset deepClone = new Dataset();
            int numberOfListsInList = data.Count;
            for (int i = 0; i < numberOfListsInList; i++)
            {
                deepClone.data.Add(new List<string>());
                for(int j = 0; j < data[i].Count; j++)
                {
                    deepClone.data[i].Add(data[i][j]);
                }
            }

            return deepClone;
        }
        public void Test(Dataset clone)
        {
            int numberOfListsInList = data.Count;
            Console.Write("Before change:");
            Console.WriteLine();
            for (int i = 0; i < numberOfListsInList; i++)
            {
                for (int j = 0; j < data[i].Count; j++)
                {
                    Console.Write(data[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.Write("Clone: ");
            Console.WriteLine();
            for (int i = 0; i < numberOfListsInList; i++)
            {
                for (int j = 0; j < clone.data[i].Count; j++)
                {
                    Console.Write(clone.data[i][j]);
                }
                Console.WriteLine();
            }

            clone.data[0][0] = "A";

            numberOfListsInList = data.Count;
            Console.WriteLine();
            Console.Write("After change:");
            Console.WriteLine();
            for (int i = 0; i < numberOfListsInList; i++)
            {
                for (int j = 0; j < data[i].Count; j++)
                {
                    Console.Write(data[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.Write("Clone: ");
            Console.WriteLine();
            numberOfListsInList = clone.data.Count;
            for (int i = 0; i < numberOfListsInList; i++)
            {
                for (int j = 0; j < clone.data[i].Count; j++)
                {
                    Console.Write(clone.data[i][j]);
                }
                Console.WriteLine();
            }
        }

        }
}
