﻿using System;

namespace Zadatak_1
{
    class Zadatak1_Program
    {
        static void Main(string[] args)
        {
            Dataset Object = new Dataset("C:\\Users\\Afaf\\Desktop\\CSV.txt");
            Dataset Clone = (Dataset)Object.Clone();

            Object.Test(Clone);


            //U ovom zadatku trebamo koristiti duboko kopiranje budući da bi se
            // korištenjem metode MemberwiseClone kopirale reference na listu
        }
    }
}
