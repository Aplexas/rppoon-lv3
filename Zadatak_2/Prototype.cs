﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_2
{
    interface Prototype
    {
        Prototype Clone();
    }
}
