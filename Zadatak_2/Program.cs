﻿using System;

namespace Zadatak_2
{
    class Zadatak2_Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator matrix = MatrixGenerator.GetInstance();
            double[][] generatedMatrix=matrix.GenerateMatrix(4, 5);
            matrix.Test(generatedMatrix,4,5);

            // Navedena metoda ima dvije odgovornosti
        }
    }
}
