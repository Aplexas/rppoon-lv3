﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;

        private Logger()
        {
            this.filePath = "C:\\Users\\Afaf\\Desktop\\Log.txt";
        }

        private Logger(string filePath)
        {
            this.filePath = filePath;
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger("C:\\Users\\Afaf\\Desktop\\Log.txt");
            }

            return instance;
        }

        public void Log(string logMessage)
        {
            using (System.IO.StreamWriter writer =
new System.IO.StreamWriter(this.filePath,true))
            {
                writer.WriteLine(logMessage);
            }
        }
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }


    }
}
