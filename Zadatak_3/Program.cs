﻿using System;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger firstLogger = Logger.GetInstance();
            Logger secondLogger = Logger.GetInstance();

            firstLogger.Log("firstLog");
            secondLogger.Log("secondLog");

            //Pisat ce u istu datoteku zato što imaju istu putanju
            //,a pisat će u istu datoteku sve dok ne promijenimo
            //putanju (filePath) log poruke.
        }
    }
}
