﻿using System;

namespace Zadata_4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification consoleNotification = new ConsoleNotification("Antonio", "RPPOON","Test123Test",
                DateTime.Now,Category.INFO,ConsoleColor.Cyan);

            NotificationManager notificationManager = new NotificationManager();

            notificationManager.Display(consoleNotification);       
        }
    }
}
